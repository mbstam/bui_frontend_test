﻿using System;
using System.Data.Sql;
using System.Xml;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Data.SqlClient;
using System.Data;

namespace BillingTestingProject
{
    [TestFixture]
    [Ignore("Ignore test case")]
    class Billing_BBA
    {
        public SqlConnection sqlConnection1;
        public SqlCommand sqlCommand1;
        public SqlDataReader reader;

        [OneTimeSetUp]
        public void TestInitialize()
        {
            sqlConnection1 = new SqlConnection("data source=S23-AS;Initial Catalog=EnerFreeDB; Integrated Security=True;MultipleActiveResultSets=True");
            sqlCommand1 = new SqlCommand();
        }

        [Test]
        [Ignore("Ignore test")]
        public void SimpleCall()
        {
            if(sqlCommand1 != null)
            {
                sqlCommand1.CommandText = "SELECT top 1 * FROM dbo.Contracten ORDER BY ContractID desc ";
                sqlCommand1.CommandType = CommandType.Text;
                sqlCommand1.Connection = sqlConnection1;

                sqlConnection1.Open();

                List<Contracten> sqldata = new List<Contracten>();

                using (reader = sqlCommand1.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        Contracten con = new Contracten();
                        con.ContractId = (int)reader["ContractID"];
                        con.LabelID = (String)reader["LabelID"];
                        sqldata.Add(con);
                    }
                }

                Assert.AreEqual(sqldata[0].ContractId, 1197352);
                Assert.AreEqual(sqldata[0].LabelID, "Budget");
               
                sqlConnection1.Close();
            }
        }
    }

    public class Contracten
    {
        public int ContractId { get; set; }
        public String LabelID { get; set; }
    }
}
