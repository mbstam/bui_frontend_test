﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace BillingTestingProject
{
  
    [Binding]
    public class BUI_BillingFeatureSteps
    {
        private IWebDriver driver = new FirefoxDriver();
        private StringBuilder verificationErrors;
        private string baseURL;
        private TimeSpan timespan = new TimeSpan(0, 2, 45);

        public void NavigateToMainPage()
        {
            try
            {
                //return to main page
                driver.FindElement(By.ClassName("brand")).Click();
                driver.Manage().Timeouts().SetPageLoadTimeout(timespan);  
                driver.FindElement(By.XPath("//a[contains(@href,'/budget/Billing')]")).Click();
                driver.SwitchTo().Alert().Accept();
            } catch (Exception e)
            {
                Console.WriteLine("Exception Dialog" + e.ToString());
            }
        }

        [Given(@"a vailid user to login in the applciation")]
        public void GivenAVailidUserToLoginInTheApplciation()
        { }
        
        [When(@"open the BUI application")]
        public void WhenOpenTheBUIApplication()
        { }
        
        [Then(@"the my user has accees to the BUI")]
        public void ThenTheMyUserHasAcceesToTheBUI()
        {
            try
            {
                Assert.AreEqual("Billing User Interface", driver.Title);
            } catch (Exception e)
            {
                Console.WriteLine("Exception Dialog" + e.ToString());
            }
           
        }

        [Given(@"a valid user in the BUI application")]
        public void GivenAValidUserInTheBUIApplication()
        {
            if (String.IsNullOrEmpty(baseURL))
            {
                driver.Navigate().GoToUrl("http://bui.acc.nutsservices.local/budget/Billing");
                baseURL = driver.Url;
            }
        }

        [When(@"open the component Eindnota Facturatie")]
        public void WhenOpenTheComponentEindnotaFacturatie()
        { 
            driver.FindElement(By.XPath("//a[contains(@href,'/budget/Billing/EndNota')]")).Click();
            //*[@id="content"]/div[2]/div[1]/p[2]/a
        }    
        
        [Then(@"All properties are availible")]
        public void ThenAllPropertiesAreAvailible()
        {
            driver.FindElement(By.LinkText("Uitval")).Click();
            driver.FindElement(By.LinkText("Facturatie")).Click();
            driver.FindElement(By.LinkText("Overzicht")).Click();
            driver.FindElement(By.LinkText("Email")).Click();
            driver.FindElement(By.LinkText("Timeslices")).Click();


            NavigateToMainPage();
        }

        [When(@"open the component Yearnota Facturatie")]
        public void WhenOpenTheComponentYearnotaFacturatie()
        {
            
            driver.Manage().Timeouts().SetPageLoadTimeout(timespan);
            driver.FindElement(By.XPath("//a[contains(@href,'/budget/Billing/YearNota')]")).Click();
        }

        [Then(@"All year nota properties are availible")]
        public void ThenAllYearNotaPropertiesAreAvailible()
        {
            driver.FindElement(By.LinkText("Uitval")).Click();
            driver.FindElement(By.LinkText("Facturatie")).Click();
            driver.FindElement(By.LinkText("Overzicht")).Click();
            driver.FindElement(By.LinkText("Email")).Click();
            driver.FindElement(By.LinkText("Timeslices")).Click();

            NavigateToMainPage();
        }

        [When(@"open the component Termijnbedragen")]
        public void WhenOpenTheComponentTermijnbedragen()
        {
            driver.FindElement(By.XPath("//a[contains(@href,'/budget/Billing/VoorschotNota')]")).Click();
        }

        [Then(@"All functions are availible")]
        public void ThenAllFunctionsAreAvailible()
        {
            ///Assert pull down jaar
            IWebElement TargetElement = driver.FindElement(By.XPath("//*[@id='invoice']/billing-bba/div/div[1]/div[2]/select")); 
            SelectElement dropdown = new SelectElement(TargetElement);
                 
            ///Assert pull down maand

            driver.Close();
        }
    }
}
