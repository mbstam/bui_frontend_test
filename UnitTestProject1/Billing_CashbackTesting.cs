﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Threading.Tasks;

namespace BillingTestingProject
{
    [TestFixture]
    class Billing_CashbackTesting
    {
        public ServiceReference2.ProcessServiceClient testservices_authenticated;
        public ServiceReference2.ProcessServiceClient testservices;
        public ServiceReference2.CashbackRequest cashbackRequest;
        public ServiceReference2.CashbackResponse cashbackResponse;

        public Cashback referenceCashback;

        [OneTimeSetUp]
        public void TestInitialize()
        {
            testservices_authenticated = new ServiceReference2.ProcessServiceClient("Authenticated");
            testservices = new ServiceReference2.ProcessServiceClient("NotAuthenticated");

            cashbackRequest = new ServiceReference2.CashbackRequest();
            cashbackRequest.ContractId = 542894;
            {
                referenceCashback = new Cashback();
                referenceCashback.description = "korting 210";
                referenceCashback.invoiceAmount = 210.00;
                referenceCashback.invoiceOn = new DateTime(2015, 06, 18, 00, 00, 00);
                referenceCashback.isInvoiced = true;
                referenceCashback.lastInvoiceDate = new DateTime(2015, 06, 18, 00, 00, 00);
                referenceCashback.multiple = false;
                referenceCashback.totalAmount = 210.00;
            }   
        }

        [Test]
        public void Test_response_from_services_auth()
        {
            if (cashbackRequest != null)
                cashbackResponse = testservices_authenticated.GetCashback(cashbackRequest);

            Assert.AreEqual(referenceCashback.description, cashbackResponse.Description);
            Assert.AreEqual(referenceCashback.invoiceAmount, cashbackResponse.InvoicedAmount);
            Assert.AreEqual(referenceCashback.invoiceOn, cashbackResponse.InvoicedOn);
            Assert.AreEqual(referenceCashback.isInvoiced, cashbackResponse.IsInvoiced);
            Assert.AreEqual(referenceCashback.lastInvoiceDate, cashbackResponse.LastInvoiceDate);
            Assert.AreEqual(referenceCashback.multiple, cashbackResponse.Multiple);
            Assert.AreEqual(referenceCashback.totalAmount, cashbackResponse.TotalAmount);
        }

        [Test]
        public void Test_response_from_services()
        {
            if (cashbackRequest != null)
                cashbackResponse = testservices.GetCashback(cashbackRequest);

            Assert.AreEqual(referenceCashback.description, cashbackResponse.Description);
            Assert.AreEqual(referenceCashback.invoiceAmount, cashbackResponse.InvoicedAmount);
            Assert.AreEqual(referenceCashback.invoiceOn, cashbackResponse.InvoicedOn);
            Assert.AreEqual(referenceCashback.isInvoiced, cashbackResponse.IsInvoiced);
            Assert.AreEqual(referenceCashback.lastInvoiceDate, cashbackResponse.LastInvoiceDate);
            Assert.AreEqual(referenceCashback.multiple, cashbackResponse.Multiple);
            Assert.AreEqual(referenceCashback.totalAmount, cashbackResponse.TotalAmount);       
        }
    }

    class Cashback
    {
        public String description { get; set;}
        public double invoiceAmount { get; set; }
        public DateTime invoiceOn { get; set; }
        public Boolean isInvoiced { get; set; }
        public DateTime lastInvoiceDate { get; set; }
        public Boolean multiple { get; set; }
        public double totalAmount { get; set; } 
    }
}
