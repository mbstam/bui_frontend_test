﻿using System;
using NUnit.Framework;
using System.Collections.Generic;



namespace BillingTestingProject
{
    [TestFixture]
    public class TariffHistoryByAdressTesting
    {
        public ServiceReference1.CustomerServiceClient testservices;
        public ServiceReference1.GetTariffHistoryByAdressRequest gettrariffhistorybyadressrequest;
        public ServiceReference1.GetTariffHistoryByAdressResponse gettariffhistorybyadressresponse;
        public DateTime dateToday;
       

        /// Test reference object 
        public ReferenceServices oldServices;
        
        [OneTimeSetUp]    
        public void TestInitialize()
        {
            testservices = new ServiceReference1.CustomerServiceClient();
            gettrariffhistorybyadressrequest = new ServiceReference1.GetTariffHistoryByAdressRequest();

                dateToday = DateTime.Now;
                gettrariffhistorybyadressrequest.AdressId = 49815;
                gettrariffhistorybyadressrequest.ContractantId = 3140746;

                oldServices = new ReferenceServices();
                List<TariffHistory> history = new List<TariffHistory>{ 
                    new TariffHistory(new DateTime(2016,01,01,00,00,00),dateToday.Date, 
                    new List<PriceComponents> { 
                        new PriceComponents("EnergieBelasting",0.12184700,0.100700,"kWh",0.02114700), 
                        new PriceComponents("OpslagDuurzameEnergie",0.00677600,0.005600,"kWh",0.00117600),
                        new PriceComponents("NormaalTarief",0.07453600,0.061600,"kWh",0.01293600),
                        new PriceComponents("DalTarief",0.05856400,0.048400,"kWh",0.01016400),
                        new PriceComponents("VasteLeveringskosten",71.8740,59.40,"jaar",12.4740),
                        new PriceComponents("TrouweKlantenKorting",-0.00999460,-0.008260,"kWh",-0.00173460)
                    }),
                    new TariffHistory(new DateTime(2015,07,01,00,00,00),new DateTime(2016,01,01,00,00,00),
                    new List<PriceComponents> { 
                        new PriceComponents("EnergieBelasting",0.231231,0.1911,"m3",0.040131), 
                        new PriceComponents("OpslagDuurzameEnergie",0.01367300,0.011300,"m3",0.00237300),
                        new PriceComponents("NormaalTarief",0.36300000,0.300000,"m3",0.06300000),
                        new PriceComponents("VasteLeveringskosten",71.8740,59.40,"jaar",12.4740),
                        new PriceComponents("TrouweKlantenKorting",-0.04999720,-0.041320,"kWh",-0.00867720)
                    })
                };
            oldServices.tariffHistory = history;
        }
       

        [Test]
        public void Start_Date_0()
        {
            if (testservices != null && gettrariffhistorybyadressrequest != null)
                gettariffhistorybyadressresponse = testservices.GetTariffHistoryByAdress(gettrariffhistorybyadressrequest);

            ServiceReference1.TariffPeriod[] tariffperiod = gettariffhistorybyadressresponse.TariffHistoryDetails.TariffHistory; 
            Assert.AreEqual(oldServices.tariffHistory[0].startdate, tariffperiod[0].Start);
        }

        [Test]
        public void End_Date_0()
        {
            if (testservices != null && gettrariffhistorybyadressrequest != null)
                gettariffhistorybyadressresponse = testservices.GetTariffHistoryByAdress(gettrariffhistorybyadressrequest);

            ServiceReference1.TariffPeriod[] tariffperiod = gettariffhistorybyadressresponse.TariffHistoryDetails.TariffHistory;
            Assert.AreEqual(oldServices.tariffHistory[0].enddate, tariffperiod[0].End);
        }

        [Test]
        public void Tarriff_0()
        {
            if (testservices != null && gettrariffhistorybyadressrequest != null)
                gettariffhistorybyadressresponse = testservices.GetTariffHistoryByAdress(gettrariffhistorybyadressrequest);

            ServiceReference1.TariffPeriod[] tariffperiod = gettariffhistorybyadressresponse.TariffHistoryDetails.TariffHistory;
            Assert.AreEqual(oldServices.tariffHistory[0].pricecomponents[0].omschrijving, tariffperiod[0].ElectricityTariffs[0].Description.ToString());
            Assert.AreEqual(oldServices.tariffHistory[0].pricecomponents[0].InclBTW, tariffperiod[0].ElectricityTariffs[0].TariffGross);
            Assert.AreEqual(oldServices.tariffHistory[0].pricecomponents[0].ExclBTW, tariffperiod[0].ElectricityTariffs[0].TariffNet);
            Assert.AreEqual(oldServices.tariffHistory[0].pricecomponents[0].BTW, tariffperiod[0].ElectricityTariffs[0].Vat);
            Assert.AreEqual(oldServices.tariffHistory[0].pricecomponents[0].Eenheid, tariffperiod[0].ElectricityTariffs[0].Unit);
        }

        [Test]
        public void Start_Date_1()
        {
            if (testservices != null && gettrariffhistorybyadressrequest != null)
                gettariffhistorybyadressresponse = testservices.GetTariffHistoryByAdress(gettrariffhistorybyadressrequest);

            ServiceReference1.TariffPeriod[] tariffperiod = gettariffhistorybyadressresponse.TariffHistoryDetails.TariffHistory;
            Assert.AreEqual(oldServices.tariffHistory[1].startdate, tariffperiod[1].Start);
        }

        [Test]
        public void End_Date_1()
        {
            if (testservices != null && gettrariffhistorybyadressrequest != null)
                gettariffhistorybyadressresponse = testservices.GetTariffHistoryByAdress(gettrariffhistorybyadressrequest);

            ServiceReference1.TariffPeriod[] tariffperiod = gettariffhistorybyadressresponse.TariffHistoryDetails.TariffHistory;
            Assert.AreEqual(oldServices.tariffHistory[1].enddate, tariffperiod[1].End);
        }

        [Test]
        public void Tarriff_1()
        {
            if (testservices != null && gettrariffhistorybyadressrequest != null)
                gettariffhistorybyadressresponse = testservices.GetTariffHistoryByAdress(gettrariffhistorybyadressrequest);

            ServiceReference1.TariffPeriod[] tariffperiod = gettariffhistorybyadressresponse.TariffHistoryDetails.TariffHistory;
            Assert.AreEqual(oldServices.tariffHistory[1].pricecomponents[0].omschrijving, tariffperiod[1].GasTariffs[0].Description.ToString());
            Assert.AreEqual(oldServices.tariffHistory[1].pricecomponents[0].InclBTW, tariffperiod[1].GasTariffs[0].TariffGross);
            Assert.AreEqual(oldServices.tariffHistory[1].pricecomponents[0].ExclBTW, tariffperiod[1].GasTariffs[0].TariffNet);
            Assert.AreEqual(oldServices.tariffHistory[1].pricecomponents[0].BTW, tariffperiod[1].GasTariffs[0].Vat);
            Assert.AreEqual(oldServices.tariffHistory[1].pricecomponents[0].Eenheid, tariffperiod[1].GasTariffs[0].Unit);
        }
    }

    public class ReferenceServices
    {    
        public List<TariffHistory> tariffHistory { get; set; }
    }

    public class TariffHistory
    {
        public TariffHistory(DateTime startdate, DateTime enddate, List<PriceComponents> pricecomponents)
        {
            this.pricecomponents = pricecomponents;
            this.startdate = startdate;
            this.enddate = enddate;
        }

        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public List<PriceComponents> pricecomponents { get; set; }
    }


    public class PriceComponents
    {
        public PriceComponents(String description, double tariffgross, double tariffnet, string unit, double vat)
        {
            omschrijving = description;
            InclBTW = tariffgross;
            ExclBTW = tariffnet;
            BTW = vat;
            Eenheid = unit;
        }

        public String omschrijving { get; set; }
        public double InclBTW;  
        public double BTW; 
        public double ExclBTW; 
        public String Eenheid; 
    }
}
